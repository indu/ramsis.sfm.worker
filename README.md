## Model worker API



## Installation

Clone the code and then change your working directory to the top level of this code.

Create a virtual/conda environment and clone the dependencies, then install them before this package using 

```
pip install -e .
```

Dependencies:
ramsis.utils: https://gitlab.seismo.ethz.ch/indu/ramsis.utils
ramsis.datamodel: https://gitlab.seismo.ethz.ch/indu/ramsis.datamodel


Install this package the same way.

Create a postgres database for the results to be saved in.
e.g.
user: postgres
password: ramsis
port: 5435
database name: ramsis_worker


initialize the database with the datamodel:
`ramsis-sfm-worker-db-init --logging-conf /home/username/repos/ramsis.sfm.worker/worker/config/logging.conf postgresql://postgres:ramsis@localhost:5435/ramsis_worker`

Altering the file for logging to suit requirements.

Start the worker:

`ramsis-sfm-worker-api --logging-conf /home/sarsonl/repos/ramsis.sfm.worker/worker/config/logging.conf --port 5007 postgresql://postgres:ramsis@localhost:5435/ramsis_worker`

Which starts the worker on port 5007 as an example.


You can now access the following routes:


POST /v1/sfm/run
with data in JSON format with the following specification:

```
{
    "data":{
        "attributes":{
            "injection_plan":"<InjectionWell future, OPTIONAL>",
            "injection_well":"<InjectionWell observed, OPTIONAL>",
            "name":"etas model",
            "description":"description of model",
            "sfm_module":"ramsis_nsfm.models.etas",
            "sfm_class":"ETASCalculation",
            "seismic_catalog":"<Catalog>",
            "geometryextent":"<Polygon>",
            "forecast_start":"2023-05-01T05:00:00",
            "forecast_end":"2024-05-01T05:00:00",
            "model_config":{
                "model_parameters":{
                    "auxiliary_start":"1992-01-01 00:00:00",
                    "timewindow_start":"1997-01-01 00:00:00",
                    "theta_0":{
                        "log10_mu":-6.21,
                        "log10_k0":-2.75,
                        "a":1.13,
                        "log10_c":-2.85,
                        "omega":-0.13,
                        "log10_tau":3.57,
                        "log10_d":-0.51,
                        "gamma":0.15,
                        "rho":0.63
                    },
                    "mc":2.3,
                    "delta_m":0.1,
                    "coppersmith_multiplier":100,
                    "earth_radius":6.3781e3,
                    "n_simulations":100
                },
                "wrapper_parameters":{
                    
                }
            }
        }
    }
}
```


Where the following defintions are given:
- InjectionWell: json format of a full injection borehole as defined by the HYDWS service https://gitlab.seismo.ethz.ch/indu/hydws/-/tree/main/hydws  of course, the injection_well and injection_plan are only required by models that take this as an input and are optional inputs.
- Catalog: A base64 encoded quakeml seismic catalog as given by a FDSN web service https://www.fdsn.org/webservices/
- Polygon: A geojson dictionary for a polygon type geometry.


GET /v1/sfm/run/<task_id>
i.e.

http://ramsis-nsfm-dev.ethz.ch:5007/v1/sfm/run/62bd788b-18d6-4170-ae2a-fb46a0b95ba9

with data being output in the following format:

```
{
    "data":{
        "id":"task_id",
        "attributes":{
            "status":"TaskCompleted",
            "status_code":200,
            "forecast":{
                "starttime":"2016-12-07T22:00:00",
                "endtime":"2016-12-08T02:00:00",
                "spatialbins":
                    {"points": "<Polygon>",
                    "geometrytype": "POLYGON",
                    "z_min": 0,
                    "z_max": 0,
                    "rates":{
                        "numberevents":{
                            "value":0.000485705038014159
                            },
                        "b":{
                            "value":3.20803244879297
                            },
                        "a":{
                            "value":-3.31362739174953
                            },
                        "mc":{
                            "value":4.4
                            }
                        }
                    "catalogs":{
                        "quakeml": "<Catalog>"
                    }
                         
                    }
                ]
            }
        }
    }
}
```



The model worker provides an API interface between seismicity models that take a pre-specified format of input, and RAMSIS.


For any questions please contact Laura at laura.sarson@sed.ethz.ch or Nicolas at nicolas.schmid@sed.ethz.ch
