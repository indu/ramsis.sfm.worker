from marshmallow import Schema, post_dump, INCLUDE
from flask import request
import collections
import enum


class StatusCode(enum.Enum):
    """
    SFM-Worker status code enum.
    """
    # codes related to worker states
    TaskAccepted = 202
    TaskProcessing = 423
    TaskError = 418
    TaskCompleted = 200
    TaskNotAvailable = 204
    # codes related to worker resource
    HTTPMethodNotAllowed = 405
    UnprocessableEntity = 422
    WorkerError = 500


class ResponseData(collections.namedtuple(
        'ResponseData', ['task_id', 'attributes'])):

    @classmethod
    def no_content(cls):
        return cls(task_id=None, attributes=None)

    @classmethod
    def accepted(cls, task_id, model_name):
        attributes = {
            'status_code': StatusCode.TaskAccepted.value,
            'status': StatusCode.TaskAccepted.name,
            'model_result_uri': f"{request.base_url}/{task_id}"}
        rval = cls(task_id=task_id, attributes=attributes)
        return rval

    @classmethod
    def from_task(cls, task):
        attributes = {
            'status_code': task.status_code,
            'status': task.status,
            'forecast': task.modelrun.resulttimebins,
            'warning': task.warning}
        print("get response from task: ", attributes)

        return cls(task_id=task.id, attributes=attributes)

    @classmethod
    def from_models(cls, models_list):
        attributes = {
            'status_code': StatusCode.TaskCompleted.value,
            'status': StatusCode.TaskCompleted.name,
            'models': models_list}

        return cls(task_id=None, attributes=attributes)

    @classmethod
    def ok(cls, task_id, forecast, warning=''):
        attributes = dict(
            status=StatusCode.TaskCompleted.name,
            status_code=StatusCode.TaskCompleted.value,
            forecast=forecast,
            warning=warning)
        return cls(task_id=task_id, attributes=attributes)

    @classmethod
    def ok_deletion(cls, task_id, warning=''):
        attributes = dict(
            status=StatusCode.TaskCompleted.name,
            status_code=StatusCode.TaskCompleted.value,
            forecast=[],
            warning=warning)
        return cls(task_id=task_id, attributes=attributes)

    @classmethod
    def error(cls, task_id, status, status_code, data={}, warning=''):

        attributes = dict(
            status_code=status_code,
            status=status,
            warning=warning)
        return cls(task_id=task_id, attributes=attributes)


class SchemaMeta(Schema):

    class Meta:
        strict = True
        ordered = True
        unknown = INCLUDE


class SchemaBase(Schema):

    class Meta:
        strict = True
        ordered = True

    @classmethod
    def remove_empty(self, data):
        """
        Filter out fields with empty (e.g. :code:`None`, :code:`[], etc.)
        values.
        """
        return {k: v for k, v in data.items() if (v and v is not {}) or
                isinstance(v, (int, float))}

    @classmethod
    def _flatten_dict(cls, data, sep='_'):
        """
        Flatten a a nested dict :code:`dict` using :code:`sep` as key
        separator.
        """
        retval = {}
        for k, v in data.items():

            if isinstance(v, dict):
                sub_k = list(v.keys())[0]
                if sub_k in ['value', 'uncertainty', 'upperuncertainty',
                             'loweruncertainty', 'confidencelevel']:
                    for sub_k, sub_v in cls._flatten_dict(v, sep).items():
                        retval[k + sep + sub_k] = sub_v
                else:
                    retval[k] = v
            else:
                retval[k] = v

        return retval

    @classmethod
    def _nest_dict(cls, data, sep='_'):
        """
        Nest a dictionary by splitting the key on a delimiter.
        """
        retval = {}
        for k, v in data.items():
            t = retval
            prev = None
            if k in ['status_code', 'sub_geometries']:
                t.setdefault(k, v)
                continue
            for part in k.split(sep):
                if prev is not None:
                    t = t.setdefault(prev, {})
                prev = part
            else:
                t.setdefault(prev, v)

        return retval

    @post_dump
    def postdump(self, data, **kwargs):
        filtered_data = self.remove_empty(data)
        nested_data = self._nest_dict(filtered_data, sep='_')
        return nested_data
