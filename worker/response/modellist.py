
from marshmallow import fields

from worker.response import SchemaBase, SchemaMeta


class ModelInfoSchema(SchemaMeta):
    name = fields.String()
    description = fields.String()
    completed_runs_count = fields.Integer()
    failed_runs_count = fields.Integer()
    model_runs_uri = fields.String()
    config = fields.Dict()


class SFMWorkerResponseModelListAttributesSchema(SchemaBase):
    """
    Schema representation for a list of model properties.
    """
    status = fields.String()
    status_code = fields.Integer()
    models = fields.Nested(ModelInfoSchema, many=True)


class SFMWorkerResponseModelListDataSchema(SchemaBase):
    """
    Schema representation fo the SFM worker response data.
    """
    attributes = fields.Nested(SFMWorkerResponseModelListAttributesSchema)


class SFMWorkerOMessageModelListSchema(SchemaBase):
    """
    Schema implementation for de-/serializing
    :py:class:`ramsis.sfm.worker.model.ModelResult`.
    """
    data = fields.Nested(SFMWorkerResponseModelListDataSchema)
