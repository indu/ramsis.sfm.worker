# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
Resource facilities for worker webservices.
"""

import importlib
import functools
import logging
import uuid

from datetime import datetime
from multiprocessing import Process, Queue
import threading

from flask import request, current_app, g
from flask import make_response as _make_response
from flask_restful import Resource
from sqlalchemy.orm.exc import NoResultFound
from ramsis.datamodel import Task as orm_Task, ModelConfig, \
    ModelRun, ResultTimeBin, SeismicForecastGrid, \
    SeismicForecastCatalog
from ramsis.io.sfm import _SFMWorkerIMessageSchema

from worker.utils.error import Error
from worker.parser import parser
from worker.task import Task
from worker.response import (
    StatusCode, ResponseData)
from ramsis.io.sfm import SFMWorkerOMessageSchema


_HTTP_OK = 200
_HTTP_NO_CONTENT = 204
_HTTP_RESOURCE_EXISTS = 409
dt_format = '%Y-%m-%dT%H:%M:%S'


# -----------------------------------------------------------------------------
class WorkerError(Error):
    """Base worker error ({})."""


class CannotCreateTaskModel(WorkerError):
    """Error while creating task model ({})."""


def make_response(msg, status_code=None,
                  serializer=SFMWorkerOMessageSchema, **kwargs):
    """
    Factory function creating :py:class:`flask.Flask.response_class.

    :param msg: Serialized message the response is created from :type msg:
    :py:class:`ramsis.response.ResponseData` or list of
        :py:class:`ramsis.reponse.ResponseData`
    :param status_code: Force HTTP status code. If :code:`None` the status code
        is extracted from :code:`msg`. If the status code is equal to
        :code:`204: an empty response is returned.
    :type status_code: int or None
    :param serializer: Schema to be used for serialization
    :type serializer: :py:class:`marshmallow.Schema`
    :param kwargs: Keyword value parameters passed to the serializer
    :returns: HTTP response
    :rtype: :py:class:`flask.Flask.response`
    """
    if status_code == _HTTP_NO_CONTENT:
        return '', _HTTP_NO_CONTENT

    try:
        if status_code is None:
            status_code = _HTTP_OK
            try:
                status_code = int(msg.attributes['status_code'])
            except (KeyError, AttributeError):
                if not isinstance(msg, list):
                    raise

        msg = dict(data=msg)
        resp = _make_response(serializer(**kwargs).dumps(msg), status_code)

    except Exception as err:
        raise WorkerError(err)

    resp.headers['Content-Type'] = 'application/json'
    return resp


def with_validated_args(func):
    """
    Method decorator providing a generic argument validation.
    """
    @functools.wraps(func)
    def decorator(self, *args, **kwargs):
        try:
            _ = self.validate_args(kwargs.get('task_id'))  # noqa
        except ValueError as err:
            self.logger.warning('Invalid argument: {}.'.format(err))

            msg = ResponseData.no_content()
            self.logger.debug('Response msg: {}'.format(msg))

            return make_response(msg, status_code=_HTTP_NO_CONTENT)

        return func(self, *args, **kwargs)

    return decorator


# -----------------------------------------------------------------------------
class RamsisWorkerBaseResource(Resource):
    """
    Base class for *RT-RAMSIS* worker resources.

    :param db: :py:class:`flask_sqlalchemy.SQLAlchemy` database instance
    :type db: :py:class:`flask_sqlalchemy.SQLAlchemy`
    """

    LOGGER = 'ramsis.sfm.worker.worker_resource'

    def __init__(self, db):
        self.logger = logging.getLogger(self.LOGGER)
        self._db = db

    def get(self):
        return 'Method not allowed.', StatusCode.HTTPMethodNotAllowed.value

    def post(self):
        return 'Method not allowed.', StatusCode.HTTPMethodNotAllowed.value

    def delete(self):
        return 'Method not allowed.', StatusCode.HTTPMethodNotAllowed.value


class ModelRunResultResource(RamsisWorkerBaseResource):
    """
    *RT-RAMSIS* seismicity forecast model (SFM) worker resource implementation.
    """

    def get_task_with_results(self, session, task_id, logging=True):
        task = session.query(orm_Task).\
            join(orm_Task.modelrun).\
            join(ModelRun.resulttimebins).\
            join(ResultTimeBin.seismicforecastgrids).\
            join(SeismicForecastGrid.seismicrates).\
            filter(orm_Task.id == task_id).\
            first()
        if task:
            # If the task does not have results with grids
            # and rates, None will be returned.
            for timebin in task.modelrun.resulttimebins:
                sum_rates = sum([len([rate for rate in grid.seismicrates])
                                 for grid in timebin.seismicforecastgrids])
                if logging:
                    self.logger.info(
                        f"{datetime.strftime(timebin.starttime, dt_format)}, "
                        f"{datetime.strftime(timebin.endtime, dt_format)}, "
                        f"Number of seismic rates, {sum_rates}")
        if not task:
            task = session.query(orm_Task).\
                join(orm_Task.modelrun).\
                join(ModelRun.resulttimebins).\
                join(ResultTimeBin.seismicforecastcatalogs).\
                join(SeismicForecastCatalog.events).\
                filter(orm_Task.id == task_id).\
                first()
            if task:
                # If the task does not have results with catalogs
                # and events, None will be returned.

                for timebin in task.modelrun.resulttimebins:
                    sum_catalogs = len(
                        [cat for cat in timebin.seismicforecastcatalogs])
                    if logging:
                        self.logger.info(
                            f"{datetime.strftime(timebin.starttime, dt_format)}, " # noqa
                            f"{datetime.strftime(timebin.endtime, dt_format)}, " # noqa
                            "Number of seismic catalogs:, "
                            f"{sum_catalogs}")
        if not task:
            # When a task has no result but does exist.
            task = session.query(orm_Task).\
                filter(orm_Task.id == task_id).\
                first()
        return task

    @with_validated_args
    def get(self, task_id):
        """
        Implementation of HTTP :code:`GET` method. Returns a specific task.
        """
        self.logger.info(
            f"Received HTTP GET request (task_id: {task_id}).")

        session = self._db.session
        try:
            task = self.get_task_with_results(session, task_id)
            if not task:
                # When a task does not exist at all.
                return make_response('', status_code=_HTTP_NO_CONTENT)
            session.expire(task)

            msg = ResponseData.from_task(task)

            return make_response(msg, status_code=_HTTP_OK)

        except Exception as err:
            raise err
        finally:
            session.close()

    @with_validated_args
    def delete(self, task_id):
        """
        Implementation of HTTP :code:`DELETE` method. Removes a specific task.
        """
        self.logger.debug(
            f"Received HTTP DELETE request (task_id: {task_id}).")

        # TODO(damb): To be checked if the task is currently processing.
        session = self._db.session
        try:
            task = self.get_task_with_results(session, task_id, logging=False)

            if not task:
                # When a task does not exist at all.
                self.logger.debug("No task exists with this id.")
                return make_response('', status_code=_HTTP_NO_CONTENT)

            session.delete(task)
            session.commit()

            return ResponseData.ok_deletion(str(task.id))

        except Exception as err:
            session.rollback()
            raise err
        else:
            self.logger.info(
                f"Task (id={task_id}) successfully removed.")
        finally:
            session.close()

    @staticmethod
    def validate_args(task_id):
        """
        Validate resource arguments.

        :param str task_id: Task identifier (must be :py:class:`uuid.UUID`
            compatible)

        :return: Task identifier
        :rtype: :py:class:`uuid.UUID`

        :raises ValueError: If an invalid value is passed
        """
        return uuid.UUID(task_id)


class LoggerThread:
    def __init__(self, queue):
        self.queue = queue

    def add_process_log(self, queue):
        self.queue = queue
        while True:
            record = self.queue.get()
            if record is None:
                break
            logger = logging.getLogger(record.name)
            logger.handle(record)


class ModelRunResource(RamsisWorkerBaseResource):
    """
    Implementation of a *stateless* *RT-RAMSIS* seismicity forecast model
    (SFM) worker resource. The resource ships a pool of worker processes.

    By default model results are written to a DB.
    """
    def __init__(self, db):

        super().__init__(db=db)

        self.queue = Queue(-1)
        self.logger_thread = LoggerThread(self.queue)
        self.workers = []

    @property
    def request_id(self):
        if getattr(g, 'request_id', None):
            return g.request_id
        raise KeyError("Missing key 'request_id' in application context.")

    def get(self):
        """
        Implementation of HTTP :code:`GET` method. Returns all available
        tasks.
        """
        self.logger.debug('Received HTTP GET request.')

        session = self._db.session
        try:
            tasks = session.query(orm_Task).\
                all()

            msg = [ResponseData.from_task(t) for t in tasks]

            return make_response(msg, status_code=_HTTP_OK,
                                 serializer=SFMWorkerOMessageSchema)

        except NoResultFound:
            return make_response('', status_code=_HTTP_NO_CONTENT)
        except Exception as err:
            session.rollback()
            raise err
        finally:
            session.close()

    def post(self):
        """
        Implementation of HTTP :code:`POST` method. Maps a task to the
        worker pool.
        """

        session = self._db.session
        data = self._parse(request, locations=('json',))
        sfm_input = data["data"]["attributes"]
        worker_config = data["data"]["worker_config"]

        # Reconstruct model
        try:
            model_config_dict = dict(
                config=sfm_input["model_parameters"],
                name=f"{worker_config['name']}_{datetime.now()}",
                description=worker_config["description"],
                sfm_module=worker_config["sfm_module"],
                sfm_function=worker_config["sfm_function"])
        except ValueError as err:
            self.logger.error(
                "Information about the model is missing, "
                f"please provide this information {err}")
            raise
        self.logger.debug(
            "Received HTTP POST request "
            f"(Model: {model_config_dict['name']!r}, "
            f"task_id: {self.request_id})")

        model_config = ModelConfig(**model_config_dict)
        session.add(model_config)

        task_id = self.request_id

        session = self._db.session

        new_task = orm_Task.new(task_id, model_config)

        session.add(new_task)
        session.commit()

        m_task = Task(current_app.config['SQLALCHEMY_DATABASE_URI'],
                      sfm_input,
                      self.queue,
                      current_app.config['PATH_LOGGING_CONFIG'],
                      current_app.config['LOG_ID'],
                      task_id=task_id)

        self.logger.debug(
            f"Executing {model_config.name!r} "
            f"at location {model_config.sfm_module!r}, "
            f"{model_config.sfm_function!r} "
            f"with config "
            f"{model_config.config} with task id: "
            f"task ({task_id}) ")

        try:
            module = importlib.import_module(model_config.sfm_module)
            model_entry_point = getattr(module, model_config.sfm_function)
        except (ImportError, AttributeError):
            raise ValueError(
                f"Unknown import module: {model_config.sfm_module}, "
                "entry_point: "
                f"{model_config.sfm_function}")

        m_task.set_model_runner(model_entry_point)

        p = Process(target=m_task)
        self.workers.append(p)
        self.logger.info(f"Start Process with target={m_task}")
        p.start()
        lp = threading.Thread(target=self.logger_thread.add_process_log,
                              args=(self.queue,))

        lp.start()
        msg = ResponseData.accepted(task_id, model_config.name)
        self.logger.debug('Task ({}) accepted.'.format(task_id))

        session.close()
        return make_response(msg)

    def _parse(self, request, locations=('json',)):
        """
        Parse the arguments for a model run. Since :code:`model_parameters`
        are implemented as a simple :py:class:`marshmallow.fields.Dict`
        i.e.

        .. code::

            model_parameters =
                marshmallow.fields.Dict(keys=marshmallow.fields.Str())

        by default no validation is performed on model parameters. However,
        overloading this template function and using a model specific
        schema allows the validation of the :code:`model_parameters`
        property.
        """
        return parser.parse(
            _SFMWorkerIMessageSchema(), request,
            locations=locations)
