# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
Parsing facilities for worker webservices.
"""
import logging
import re
import dateutil.parser
from marshmallow import fields
from webargs.flaskparser import abort
from webargs.flaskparser import parser as _parser
from functools import partial

from worker.response import StatusCode
from ramsis.io.sfm import (
    validate_ph,
    validate_longitude,
    validate_latitude)

logger = logging.getLogger(__name__)

Latitude = partial(fields.Float, validate=validate_latitude)
RequiredLatitude = partial(Latitude, required=True)
Longitude = partial(fields.Float, validate=validate_longitude)
RequiredLongitude = partial(Longitude, required=True)
FluidPh = partial(fields.Float, validate=validate_ph)

MODEL_CONFIG_KEYS = ["runner_module", "runner_function_name"]

# from marshmallow (originally from Django)
_iso8601_re = re.compile(
    r'(?P<year>\d{4})-(?P<month>\d{1,2})-(?P<day>\d{1,2})'
    r'[T ](?P<hour>\d{1,2}):(?P<minute>\d{1,2})'
    r'(?::(?P<second>\d{1,2})(?:\.(?P<microsecond>\d{1,6})\d{0,6})?)?'
    # tzinfo must not be available
    r'(?P<tzinfo>(?!\Z|[+-]\d{2}(?::?\d{2})?))?$')


def datetime_to_isoformat(dt, localtime=False, *args, **kwargs):
    """
    Convert a :py:class:`datetime.datetime` object to a ISO8601 conform string.
    :param datetime.datetime dt: Datetime object to be converted
    :param bool localtime: The parameter is ignored
    :returns: ISO8601 conform datetime string
    :rtype: str
    """
    # ignores localtime parameter
    return dt.isoformat(*args, **kwargs)


def string_to_datetime(datestring, use_dateutil=True):
    """
    Parse a datestring from a string specified by the FDSNWS datetime
    specification.
    :param str datestring: String to be parsed
    :param bool use_dateutil: Make use of the :code:`dateutil` package if set
        to :code:`True`
    :returns: Datetime
    :rtype: :py:class:`datetime.datetime`
    See: http://www.fdsn.org/webservices/FDSN-WS-Specifications-1.1.pdf
    """
    IGNORE_TZ = True

    if not _iso8601_re.match(datestring):
        raise ValueError('Not a valid ISO8601-formatted string.')
    return dateutil.parser.parse(datestring, ignoretz=IGNORE_TZ)


def truncate_datetime_string(datestring):
    """
    Remove any milliseconds data. Will remove timezone data too, so
    should only be used with UTC datetimes.
    """
    return datestring.split('.')[0]


class UTCDateTime(fields.DateTime):
    """
    The class extends marshmallow standard DateTime with a FDSNWS *datetime*
    format.
    The FDSNWS *datetime* format is described in the `FDSN Web Service
    Specifications
    <http://www.fdsn.org/webservices/FDSN-WS-Specifications-1.1.pdf>`_.
    """

    SERIALIZATION_FUNCS = fields.DateTime.SERIALIZATION_FUNCS.copy()

    DESERIALIZATION_FUNCS = fields.DateTime.DESERIALIZATION_FUNCS.copy()

    SERIALIZATION_FUNCS['utc_isoformat'] = datetime_to_isoformat
    DESERIALIZATION_FUNCS['utc_isoformat'] = string_to_datetime


class TruncatedDateTime(fields.DateTime):
    """
    The class extends marshmallow standard DateTime with a FDSNWS *datetime*
    format.
    The FDSNWS *datetime* format is described in the `FDSN Web Service
    Specifications
    <http://www.fdsn.org/webservices/FDSN-WS-Specifications-1.1.pdf>`_.
    """

    SERIALIZATION_FUNCS = fields.DateTime.SERIALIZATION_FUNCS.copy()

    DESERIALIZATION_FUNCS = fields.DateTime.DESERIALIZATION_FUNCS.copy()

    SERIALIZATION_FUNCS['utc_isoformat'] = datetime_to_isoformat
    DESERIALIZATION_FUNCS['utc_isoformat'] = truncate_datetime_string


class TupleField(fields.Field):

    def __init__(self, entries, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._entries = entries

    def _deserialize(self, value, attr=None, data=None):
        return tuple(
            field._deserialize(val, attr, data) for field, val in
            zip(self._entries, value))


@_parser.error_handler
def handle_request_parsing_error(err, req, schema, error_status_code,
                                 error_headers):
    """
    Webargs error handler that uses Flask-RESTful's abort function
    to return a JSON error response to the client.
    """
    abort(StatusCode.UnprocessableEntity.value,
          errors=err.messages)


parser = _parser
