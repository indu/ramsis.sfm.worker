# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
ModelRun facilities.
"""
import traceback
import functools
import logging
import uuid
import logging.handlers

from sqlalchemy.orm.exc import NoResultFound

from ramsis.io.worker import SFMResultDeserializer
from worker.utils.error import ErrorWithTraceback
from worker.utils import escape_newline, ContextLoggerAdapter
from worker.response import StatusCode
from worker.utils.app_utils import setup_logger
from worker.db.utils import task_from_db, session_handler


class TaskError(ErrorWithTraceback):
    """Base task error ({})."""


class NoTaskModel(TaskError):
    """Task model not available ({})."""


# -----------------------------------------------------------------------------
def with_exception_handling(func):
    """
    Method decorator catching unhandled
    :py:class:`worker.task.Task` exceptions. Exceptions are simply
    logged.
    """
    @functools.wraps(func)
    def decorator(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except Exception as err:
            with session_handler(self._db_url) as session:
                task = task_from_db(session, self._task_id)

                msg = 'TaskError ({}): {}: {}'.format(type(self).__name__,
                                                      type(err).__name__, err)
                task.status = msg
                task.status_code = StatusCode.WorkerError.value
                session.commit()
                self.logger.critical(traceback.format_exc())

                self.logger.critical(escape_newline(msg))

    return decorator


# -----------------------------------------------------------------------------
class Task(object):
    """
    :py:class:`Task` implementation running
    :py:class: `worker.runner.run_model` Provides a top-level
    class for easier pickling. In addition acts as a controller for the task's
    *actual* model (not to be confused with the scientific model).

    :py:class:`worker.runner.run_model.ModelResult`s are written to a
    database.

    .. note::

        In order to obtain DB access a :py:class:`Task` creates both a new DB
        engine and session which are independent from the session handling
        provided by
        [FlaskSQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/). This
        approach simplifies how DB connections are handled when a task is
        executed by means of a forked process.

    :param str db_url: DB URL indicating the database dialect and connection
        arguments
    :param model_runner_obj: :py:class:`worker.runner.ModelRunnerObj`
        instance to be run, which calls a model as set in the config.
    :param task_id: Task identifier
    :type task_id: :py:class:`ùuid.UUID`
    :param kwargs: Keyword value parameters used when running the
        model.
    """

    LOGGER = 'ramsis.sfm.worker.task'

    def __init__(self, db_url, sfm_input,
                 queue, logging_config_path,
                 log_id, task_id=None):

        self._db_url = db_url
        self._task_id = task_id if task_id is not None else uuid.uuid4()
        self._model_runner_obj = None
        self.sfm_input = sfm_input
        self.queue = queue
        self.logging_config_path = logging_config_path
        self.log_id = log_id

        setup_logger(self.logging_config_path, self.log_id)
        self._logger = logging.getLogger(self.LOGGER)
        self.logger = ContextLoggerAdapter(self._logger,
                                           {'ctx': self._task_id})

    def set_model_runner(self, model_runner_obj):
        self._model_runner_obj = model_runner_obj

    @property
    def id(self):
        return self._task_id

    @with_exception_handling
    def __call__(self):

        with session_handler(self._db_url) as session:
            try:
                m_task = task_from_db(session, self.id)
                m_task.status = StatusCode.TaskProcessing.name
                m_task.status_code = StatusCode.TaskProcessing.value
                session.commit()

            except NoResultFound as err:
                self.logger.warning(
                    f"Task unavailable ({err}). Nothing to be done.")
                return None
            except Exception as err:
                session.rollback()
                raise NoTaskModel(err)

        # Return a 'dataframe' style object
        retval = self._model_runner_obj(self.sfm_input)

        with session_handler(self._db_url) as session:
            try:
                # Successful result returned
                m_task = task_from_db(session, self.id)

                m_task.status = StatusCode.TaskCompleted.name

                m_task.status_code = StatusCode.TaskCompleted.value

                deserializer = SFMResultDeserializer()
                forecast_time_bins = deserializer._deserialize(
                    retval)
                m_task.modelrun.resulttimebins = forecast_time_bins
                timebins = m_task.modelrun.resulttimebins

                try:
                    forecast_grids = timebins[0].seismicforecastgrids
                    catalogs = timebins[0].seismicforecastcatalogs
                    if catalogs:
                        self.logger.info(
                            "Number of catalogs in first time/space bin: "
                            f"{len(catalogs)}")
                    if forecast_grids:
                        rates = forecast_grids[0].seismicrates
                        self.logger.info(
                            "Number of rates in first time/space bin: "
                            f"{len(rates)}")
                except Exception as err:
                    self.logger.warning(
                        f"Error found when logging results: {err}")
                    pass

                session.commit()

            except NoResultFound as err:
                self.logger.warning(
                    f"Task unavailable ({err}). Unable to write results.")
            except Exception as err:
                self.logger.info("rolling back.")
                session.rollback()
                raise err
            else:
                self.logger.info("Task successfully written. task status: "
                                 f"{m_task.status}, {m_task.status_code}")

    def __repr__(self):
        return '<{}(id={})>'.format(type(self).__name__, self.id)
