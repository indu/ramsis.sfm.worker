# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
General purpose ramsis.sfm.workers utilities
"""
import collections
import argparse
import logging
import pkg_resources


def get_version(namespace_pkg_name=None):
    """
    fetch version string

    :param str namespace_pkg_name: distribution name of the namespace package
    :returns: version string
    :rtype: str
    """
    try:
        # distributed as namespace package
        if namespace_pkg_name:
            return pkg_resources.get_distribution(namespace_pkg_name).version
        raise
    except Exception:
        return pkg_resources.get_distribution("ramsis.sfm.worker").version


def escape_newline(s):
    """
    Escape newline characters.

    :param str s: String to be processed.
    """
    return s.replace('\n', '\\n').replace('\r', '\\r')


def url(url):
    """
    check if SQLite URL is absolute.
    """
    if (url.startswith('sqlite:') and not
            (url.startswith('////', 7) or url.startswith('///C:', 7))):
        raise argparse.ArgumentTypeError('SQLite URL must be absolute.')
    return url


class ContextLoggerAdapter(logging.LoggerAdapter):
    """
    Adapter expecting the passed in dict-like object to have a 'ctx' key, whose
    value in brackets is prepended to the log message.
    """
    CONTEXT_DELIMITER = '::'

    def process(self, msg, kwargs):
        if isinstance(self.extra['ctx'], collections.abc.Sequence):
            prefix = self.CONTEXT_DELIMITER.join(
                f"{c}" for c in self.extra['ctx'])
        else:
            prefix = '%s' % self.extra['ctx']
        return '[%s] %s' % (prefix, msg), kwargs
