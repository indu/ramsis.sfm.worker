import enum


class ExitCode(enum.Enum):
    EXIT_SUCCESS = 0
    EXIT_WARNING = 1
    EXIT_ERROR = 2


class Error(Exception):
    """Error base class"""
    exit_code = ExitCode.EXIT_ERROR.value
    traceback = False

    def __init__(self, *args):
        super().__init__(*args)
        self.args = args

    def get_message(self):
        return type(self).__doc__.format(*self.args)

    __str__ = get_message


class ErrorWithTraceback(Error):
    """Error with traceback."""
    traceback = True
