import os
import argparse
import configparser
import logging
import logging.config
import logging.handlers
import sys
import yaml

from collections import OrderedDict

from worker.utils.error import ErrorWithTraceback, ExitCode


def realpath(p):
    return os.path.realpath(os.path.expanduser(p))


def real_file_path(path):
    """
    check if file exists
    :returns: realpath in case the file exists
    :rtype: str
    :raises argparse.ArgumentTypeError: if file does not exist
    """
    path = realpath(path)
    if not os.path.isfile(path):
        raise argparse.ArgumentTypeError(
            '{0!r} is not a valid file path.'.format(path))
    return path


class CustomParser(argparse.ArgumentParser):
    """
    Custom argument parser
    """

    def error(self, message):
        sys.stderr.write('USAGE ERROR: %s\n' % message)
        self.print_help()
        sys.exit(ExitCode.EXIT_ERROR.value)


class AppError(ErrorWithTraceback):
    """Base application error"""


class LoggingConfOptionAlreadyAvailable(AppError):
    """CLI option '--logging-conf' already defined. ({})."""


class App(object):
    """
    Implementation of a configurable application.
    """

    VERSION = None

    def __init__(self, log_id='RAMSIS'):
        self.parser = None
        self.args = None
        self.log_id = log_id
        self.logger = None
        self.logger_configured = False

    def configure(
            self,
            path_default_config,
            config_section=None,
            positional_required_args=[],
            capture_warnings=True,
            **kwargs):
        """
        Configure the application.

        :param str path_default_config: Path to the default configuration file.
        :param str config_section: Name of the configuration section in the
            configuration file.
        :param list positional_required_args: List of *argparse* `dest` values
            of positional_required_args. If such a parameter was found in the
            configuration file it is **always** appended to the list of
            defaults.
        :param bool capture_warnings: Capture warnings.
        """
        c_parser = self._build_configfile_parser(path_default_config)
        args, remaining_argv = c_parser.parse_known_args()

        defaults = {}

        if (path_default_config is not None and
                os.path.isfile(path_default_config) and config_section):
            config_parser = configparser.ConfigParser(**kwargs)
            config_parser.read(args.config_file)
            env_dict = None
            interpolation = kwargs.get('interpolation',
                                       configparser.BasicInterpolation())
            if (interpolation and
                isinstance(interpolation,
                           configparser.BasicInterpolation)):
                # filter out variables containing a '%'; else interpolation
                # fails
                env_dict = {k: v for k, v in os.environ.items()
                            if v.find('%') == -1}
            try:
                defaults = dict(config_parser.items(
                    config_section, vars=env_dict))
            except Exception as err:
                import warnings
                warnings.warn(
                    "Exception while parsing config file: {}.".format(err))

        # if config_section:
        #    config_parser = configparser.ConfigParser()
        #    config_parser.read(args.config_file)
        # try:
        #    defaults = dict(config_parser.items(config_section))
        # except Exception:
        #    pass
        self.parser = self.build_parser(parents=[c_parser])

        # XXX(damb): --version|-V flag
        if self.VERSION:
            try:
                self.parser.add_argument('--version', '-V', action='version',
                                         version='%(prog)s version ' +
                                         self.VERSION)
            except argparse.ArgumentError:
                pass

        # XXX(damb): Make sure that the default logger has an argument
        # path_logging_conf
        try:
            self.parser.add_argument('--logging-conf',
                                     dest='path_logging_conf',
                                     metavar='LOGGING_CONF',
                                     type=real_file_path,
                                     help="path to a logging configuration "
                                     "file")
        except argparse.ArgumentError as err:
            raise LoggingConfOptionAlreadyAvailable(err)

        # fetch positional arguments from config file and keep them ordered
        positional_required = OrderedDict()
        for dest in positional_required_args:
            try:
                positional_required[dest] = defaults[dest]
                defaults.pop(dest)
            except KeyError:
                pass

        # set defaults taken from configuration file
        self.parser.set_defaults(**defaults)
        # set the config_file default explicitly since adding the c_parser as a
        # parent would change the args.config_file to default=PATH_RAMSIS_CONF
        # within the child parser
        self.parser.set_defaults(config_file=args.config_file)

        try:
            def _error(msg):
                sys.exit(ExitCode.EXIT_ERROR.value)

            error_func = self.parser.error
            self.parser.error = _error
            self.args = self.parser.parse_args(remaining_argv)

        except SystemExit as err:
            # append positional required arguments if parsing at a first glance
            # failed
            if err.code == 0:
                # terminate normally (for e.g. [-h|--help])
                sys.exit(ExitCode.EXIT_SUCCESS.value)

            for v in positional_required.values():
                remaining_argv.append(v)

            self.parser.error = error_func
            self.args = self.parser.parse_args(remaining_argv)

        self._setup_logger()
        if not self.logger_configured:
            self.logger = logging.getLogger()
            self.logger.addHandler(logging.NullHandler())

        logging.captureWarnings(capture_warnings)

        return self.args

    def _build_configfile_parser(self, path_default_config):
        c_parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            add_help=False)

        c_parser.add_argument(
            "-c", "--config", dest="config_file",
            default=path_default_config,
            metavar="PATH",
            type=realpath,
            help="path to RT-RAMSIS configuration file " +
                 "(default: '%(default)s')")

        return c_parser

    def _setup_logger(self):
        """
        Initialize the logger of the application.
        """
        if self.args.path_logging_conf:
            try:
                if self.args.path_logging_conf.endswith('conf'):
                    logging.config.fileConfig(self.args.path_logging_conf)
                elif self.args.path_logging_conf.endswith('yaml'):
                    with open(self.args.path_logging_conf, 'r') as conf:
                        log_cfg = yaml.safe_load(conf)
                    logging.config.dictConfig(log_cfg)
                self.logger = logging.getLogger()
                self.logger_configured = True
                self.logger.info('Using logging configuration read from "%s"',
                                 self.args.path_logging_conf)

            except Exception as err:
                print('WARNING: Setup logging failed for "%s" with "%s".' %
                      (self.args.path_logging_conf, err), file=sys.stderr)
                self._setup_fallback_logger()
                self.logger.warning('Setup logging failed with %s. '
                                    'Using fallback logging configuration.' %
                                    err)

    def _setup_fallback_logger(self):
        """setup a fallback logger"""
        # NOTE(damb): Provide fallback syslog logger.
        self.logger = logging.getLogger()
        fallback_handler = logging.handlers.SysLogHandler('/dev/log',
                                                          'local0')
        fallback_handler.setLevel(logging.WARN)
        fallback_formatter = logging.Formatter(
            fmt=("<" + self.log_id +
                 "> %(asctime)s %(levelname)s %(name)s %(process)d "
                 "%(filename)s:%(lineno)d - %(message)s"),
            datefmt="%Y-%m-%dT%H:%M:%S%z")
        fallback_handler.setFormatter(fallback_formatter)
        self.logger.addHandler(fallback_handler)
        self.logger_configured = True

    def build_parser(self, parents=[]):
        """
        Abstract method to configure a parser. This method must be implemented
        by clients.

        :param list parents: list of parent parsers
        :returns: parser
        :rtype: :py:class:`argparse.ArgumentParser`
        """
        raise NotImplementedError

    def run(self):
        """
        Abstract method to run application. The method must be implemented by
        clients.
        """
        raise NotImplementedError


def setup_logger(path_logging_conf, log_id='RAMSIS'):
    """
    Initialize the logger for a process.
    """
    if path_logging_conf:
        try:
            if path_logging_conf.endswith('conf'):
                logging.config.fileConfig(path_logging_conf)
            elif path_logging_conf.endswith('yaml'):
                with open(path_logging_conf, 'r') as conf:
                    log_cfg = yaml.safe_load(conf)
                logging.config.dictConfig(log_cfg)

        except Exception as err:
            print('WARNING: Setup logging failed for "%s" with "%s".' %
                  (path_logging_conf, err), file=sys.stderr)
            setup_fallback_logger(log_id)


def setup_fallback_logger(log_id):
    """setup a fallback logger"""
    logger = logging.getLogger()
    fallback_handler = logging.handlers.SysLogHandler('/dev/log',
                                                      'local0')
    fallback_handler.setLevel(logging.WARN)
    fallback_formatter = logging.Formatter(
        fmt=("<" + log_id +
             "> %(asctime)s %(levelname)s %(name)s %(process)d "
             "%(filename)s:%(lineno)d - %(message)s"),
        datefmt="%Y-%m-%dT%H:%M:%S%z")
    fallback_handler.setFormatter(fallback_formatter)
    logger.addHandler(fallback_handler)
