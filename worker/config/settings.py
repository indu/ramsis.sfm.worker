"""
Set API configuration
"""
from os.path import join, abspath, dirname

dirpath = dirname(abspath(__file__))

PATH_RAMSIS_WORKER_CONFIG = '/var/www/em1/config/worker_config'
RAMSIS_WORKER_DB_CONFIG_SECTION = 'WORKER'
PATH_RUN = '/sfm/run'
LOGGING_CONF = join(dirpath, 'logging.conf')
RAMSIS_WORKER_PORT = 5000
