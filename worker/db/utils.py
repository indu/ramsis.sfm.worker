import logging
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from sqlalchemy.pool import NullPool
from ramsis.datamodel import Task
import contextlib

logger = logging.getLogger(__name__)


def connect_to_db(connection_string: str):
    engine = create_engine(connection_string, poolclass=NullPool)
    session = Session(engine)
    return session


@contextlib.contextmanager
def session_handler(connection_string):
    session = connect_to_db(connection_string)
    yield session
    session.rollback()
    session.close()


def task_from_db(session, task_id):
    return session.query(Task).\
        filter(Task.id == task_id).\
        one()
