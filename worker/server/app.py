# Copyright 2018, ETH Zurich - Swiss Seismological Service SED
"""
RAMSIS SFM-Worker.
"""

import sys
import traceback
import multiprocessing

from worker.utils.error import Error, ExitCode
from worker.config import settings
from worker.server import create_app
from worker.utils import escape_newline, url
from worker.utils.app_utils import CustomParser, App, AppError


class WorkerWebserviceBase(App):
    """
    Base production implementation of HYDWS.
    """
    PROG = 'sfm_api'

    def build_parser(self, parents=[]):
        """
        Set up the commandline argument parser.

        :param list parents: list of parent parsers
        :returns: parser
        :rtype: :py:class:`argparse.ArgumentParser`
        """

        parser = CustomParser(
            prog=self.PROG,
            description='Launch Worker Web Service.',
            parents=parents)

        parser.add_argument('-p', '--port', metavar='PORT', type=int,
                            help='server port')

        # positional arguments
        parser.add_argument('db_url', type=url, metavar='URL',
                            help=('DB URL indicating the database dialect and '
                                  'connection arguments. For SQlite only a '
                                  'absolute file path is supported.'))
        parser.set_defaults(logging_conf=settings.LOGGING_CONF)

        return parser

    # build_parser ()

    def run(self):
        """
        Run application.
        """
        exit_code = ExitCode.EXIT_SUCCESS
        try:
            self.logger.info('{}'.format(self.PROG))
            self.logger.debug('Configuration: {!r}'.format(self.args))

            app = self.setup_app()

            try:
                from mod_wsgi import version  # noqa
                self.logger.info('Serving with mod_wsgi.')
            except Exception:
                pass

            return app

        except Error as err:
            self.logger.error(err)
            exit_code = ExitCode.EXIT_ERROR
        except Exception as err:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            self.logger.critical('Local Exception: %s' % err)
            self.logger.critical(
                'Traceback information: ' + repr(traceback.format_exception(
                    exc_type, exc_value, exc_traceback)))
            exit_code = ExitCode.EXIT_ERROR

        sys.exit(exit_code)

    def setup_app(self):
        """
        Setup and configure the Flask app with its API.

        :returns: The configured Flask application instance.
        :rtype :py:class:`flask.Flask`:
        """
        app_config = {
            'PORT': self.args.port,
            'SQLALCHEMY_DATABASE_URI': self.args.db_url,
            'PATH_LOGGING_CONFIG': self.args.path_logging_conf,
            'LOG_ID': self.log_id
        }
        app = create_app(config_dict=app_config)

        return app


class WorkerWebserviceTest(WorkerWebserviceBase):
    """
    A webservice providing access to seismicity forecasting models.
    """

    def build_parser(self, parents=[]):
        """
        Set up the commandline argument parser.

        :param list parents: list of parent parsers
        :returns: parser
        :rtype: :py:class:`argparse.ArgumentParser`
        """
        parser = CustomParser(
            prog="ramsis-sfm-worker",
            description='Launch worker webservice.',
            parents=parents)
        # optional arguments
        parser.add_argument('-p', '--port', metavar='PORT', type=int,
                            default=settings.RAMSIS_WORKER_PORT,
                            help='server port')
        # positional arguments
        parser.add_argument('db_url', type=url, metavar='URL',
                            help=('DB URL indicating the database dialect and '
                                  'connection arguments. For SQlite only a '
                                  'absolute file path is supported.'))

        return parser

    def run(self):
        """
        Run application.
        """
        exit_code = ExitCode.EXIT_SUCCESS
        try:
            app = self.setup_app()
            self.logger.debug('Routes configured: {}'.format(
                escape_newline(str(app.url_map))))
            self.logger.info('Serving with local WSGI server.')
            app.run(threaded=True, debug=True, host="0.0.0.0",
                    port=self.args.port)

        except Error as err:
            self.logger.error(err)
            exit_code = ExitCode.EXIT_ERROR
        except Exception as err:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            self.logger.critical('Local Exception: %s' % err)
            self.logger.critical('Traceback information: ' + # noqa
                                 repr(traceback.format_exception(
                                     exc_type, exc_value, exc_traceback)))
            exit_code = ExitCode.EXIT_ERROR

        sys.exit(exit_code.value)


WorkerWebservice = WorkerWebserviceBase


# ----------------------------------------------------------------------------
def _main(app):
    """
    main function for model worker webservice
    """
    # Spawn required instead of default fork for logging
    # to work. This is because plain forking copies logging locks
    # in an acquired state, leading to deadlocked processes.
    try:
        multiprocessing.set_start_method('spawn')
    except RuntimeError:
        pass

    try:
        app.configure(
            settings.PATH_RAMSIS_WORKER_CONFIG,
            positional_required_args=['db_url'])
    except AppError as err:
        print('ERROR: Application configuration failed "%s".' % err,
              file=sys.stderr)
        # handle errors during the application configuration
        sys.exit(ExitCode.EXIT_ERROR.value)

    return app.run()


def main_prod():
    return _main(WorkerWebservice(log_id='RAMSIS-SFM'))


def main_test():
    return _main(WorkerWebserviceTest(log_id='RAMSIS-SFM-TEST'))


main = main_prod


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    main_test()
