# Copyright 2018, ETH Zurich - Swiss Seismological Service SED
"""
resource facilities.
"""
from flask_restful import Api

from worker.server import db
from worker.server.v1 import blueprint
from worker.resource import (ModelRunResource,
                             ModelRunResultResource)
from worker.config.settings import PATH_RUN

api_v1 = Api(blueprint)


class ModelRunAPI(ModelRunResource):

    LOGGER = 'ramsis.worker.model_run_api'


class ModelRunResultAPI(ModelRunResultResource):

    LOGGER = 'ramsis.worker.model_run_result_api'


api_v1.add_resource(ModelRunAPI,
                    f"{PATH_RUN}",
                    resource_class_kwargs={
                        'db': db})

api_v1.add_resource(ModelRunResultAPI,
                    f"{PATH_RUN}/<task_id>",
                    resource_class_kwargs={
                        'db': db})
