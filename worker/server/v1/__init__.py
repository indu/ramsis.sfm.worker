# Copyright 2018, ETH Zurich - Swiss Seismological Service SED
"""
SFM-Worker blueprint v1.
"""

from flask import Blueprint

API_VERSION_V1 = 1
API_VERSION = API_VERSION_V1

blueprint = Blueprint('v1', __name__)

from worker.server.v1 import routes # noqa
