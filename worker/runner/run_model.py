import logging

from ramsis.sfm import model_runner
from worker.utils.error import Error
from worker.utils import ContextLoggerAdapter
from worker.response import ResponseData


class ModelError(Error):
    """Base model error ({})."""


class InvalidConfiguration(ModelError):
    """Invalid configuration ({})."""


class ModelRunnerObject:
    """
    Object created to start the model runner in a seperate
    process with proper error handling and logging.
    """
    LOGGER = 'worker.runner.run_model'
    NAME = 'MODEL_RUNNER_OBJECT'
    DESCRIPTION = 'Model Runner Interaction Object.'

    def __init__(self, model, model_class, task_id, **kwargs):
        self.model = model
        self.model_class = model_class
        self.task_id = task_id

        self._logger = logging.getLogger(self.LOGGER)
        ctx_logger = ({'ctx': [self.task_id, self.name]})
        self.logger = ContextLoggerAdapter(self._logger, ctx_logger)

        self._stdout = None
        self._stderr = None

    @property
    def name(self):
        return self.model.name

    @property
    def stdout(self):
        return self._stdout

    @property
    def stderr(self):
        return self._stderr

    def _run(self, sfm_input):

        retval = model_runner(self.model_class, sfm_input)
        return ResponseData.ok(self.task_id, retval)

    def __call__(self, sfm_input):
        """
        The concept of  *task* is implemented by means of calling or rather
        executing a :py:class:`Model` instance, respectively.
        """
        model_dataframe = self._run(sfm_input)
        return model_dataframe

    def __getstate__(self):
        # prevent pickling errors for loggers
        d = dict(self.__dict__)
        if '_logger' in d.keys():
            d['_logger'] = d['_logger'].name
        if 'logger' in d.keys():
            del d['logger']
        return d

    def __setstate__(self, d):
        if '_logger' in d.keys():
            d['_logger'] = logging.getLogger(d['_logger'])
            d['logger'] = ContextLoggerAdapter(
                d['_logger'],
                {'ctx': [d['task_id'], d['model'].name]})
            self.__dict__.update(d)

    def __repr__(self):
        return '<{}(name={})>'.format(type(self).__name__, self.NAME)
